﻿ 
 
 
 
<InfoViz>
TEST PLAN
Version <1.0>

VERSION HISTORY
[Provide information on how the development and distribution of the Test Plan, up to the final point of approval, was controlled and tracked.  Use the table below to provide the version number, the author implementing the version, the date of the version, the name of the person approving the version, the date that particular version was approved, and a brief description of the reason for creating the revised version.]
Version
#
Implemented
By
Revision
Date
Approved
By
Approval
Date
Reason
1.0
<Author name>
<mm/dd/yy>
<name>
<mm/dd/yy>
Test Plan draft


UP Template Version: 12/31/07
Note to the Author

[This document is a template of a Test Plan document for a project. The template includes instructions to the author, boilerplate text, and fields that should be replaced with the values specific to the project.
 
Blue italicized text enclosed in square brackets ([text]) provides instructions to the document author, or describes the intent, assumptions and context for content included in this document.
 
Blue italicized text enclosed in angle brackets (<text>) indicates a field that should be replaced with information specific to a particular project.
 
Text and tables in black are provided as boilerplate examples of wording and formats that may be used or modified as appropriate to a specific project.  These are offered only as suggestions to assist in developing project documents; they are not mandatory formats.
 
When using this template for your project document, it is recommended that you follow these steps:
1. Replace all text enclosed in angle brackets (i.e., <Project Name>) with the correct field values. These angle brackets appear in both the body of the document and in headers and footers.  To customize fields in Microsoft Word (which display a gray background when selected):
1. Select File>Properties>Summary and fill in the Title field with the Document Name and the Subject field with the Project Name.  
2. Select File>Properties>Custom and fill in the Last Modified, Status, and Version fields with the appropriate information for this document. 
3. After you click OK to close the dialog box, update the fields throughout the document with these values by selecting Edit>Select All (or Ctrl-A) and pressing F9.  Or you can update an individual field by clicking on it and pressing F9. This must be done separately for Headers and Footers.
2. Modify boilerplate text as appropriate to the specific project. 
3. To add any new sections to the document, ensure that the appropriate header and body text styles are maintained.  Styles used for the Section Headings are Heading 1, Heading 2 and Heading 3. Style used for boilerplate text is Body Text.
4. To update the Table of Contents, right-click and select “Update field” and choose the option- “Update entire table”
5. Before submission of the first draft of this document, delete this “Notes to the Author” page and all instructions to the author, which appear throughout the document as blue italicized text enclosed in square brackets.]

TABLE OF CONTENTS
1 INTRODUCTION 7
1.1 Purpose of The Test Plan Document 7
2 COMPATIBILITY TESTING 7
2.1 Test Risks / Issues 7
2.2 Items to be Tested / Not Tested 7
2.3 Test Approach(s) 7
2.4 Test Regulatory / Mandate Criteria 7
2.5 Test Pass / Fail Criteria 7
2.6 Test Entry / Exit Criteria 7
2.7 Test Deliverables 8
2.8 Test Suspension / Resumption Criteria 8
2.9 Test Environmental / Staffing / Training Needs 8
3 CONFORMANCE TESTING 8
3.1 Test Risks / Issues 8
3.2 Items to be Tested / Not Tested 8
3.3 Test Approach(s) 8
3.4 Test Regulatory / Mandate Criteria 8
3.5 Test Pass / Fail Criteria 8
3.6 Test Entry / Exit Criteria 8
3.7 Test Deliverables 9
3.8 Test Suspension / Resumption Criteria 9
3.9 Test Environmental / Staffing / Training Needs 9
4 FUNCTIONAL TESTING 9
4.1 Test Risks / Issues 9
4.2 Items to be Tested / Not Tested 9
4.3 Test Approach(s) 9
4.4 Test Regulatory / Mandate Criteria 9
4.5 Test Pass / Fail Criteria 9
4.6 Test Entry / Exit Criteria 9
4.7 Test Deliverables 10
4.8 Test Suspension / Resumption Criteria 10
4.9 Test Environmental / Staffing / Training Needs 10
5 LOAD TESTING 10
5.1 Test Risks / Issues 10
5.2 Items to be Tested / Not Tested 10
5.3 Test Approach(s) 10
5.4 Test Regulatory / Mandate Criteria 10
5.5 Test Pass / Fail Criteria 10
5.6 Test Entry / Exit Criteria 10
5.7 Test Deliverables 11
5.8 Test Suspension / Resumption Criteria 11
5.9 Test Environmental / Staffing / Training Needs 11
6 PERFORMANCE TESTING 11
6.1 Test Risks / Issues 11
6.2 Items to be Tested / Not Tested 11
6.3 Test Approach(s) 11
6.4 Test Regulatory / Mandate Criteria 11
6.5 Test Pass / Fail Criteria 11
6.6 Test Entry / Exit Criteria 11
6.7 Test Deliverables 12
6.8 Test Suspension / Resumption Criteria 12
6.9 Test Environmental / Staffing / Training Needs 12
7 REGRESSION TESTING 12
7.1 Test Risks / Issues 12
7.2 Items to be Tested / Not Tested 12
7.3 Test Approach(s) 12
7.4 Test Regulatory / Mandate Criteria 12
7.5 Test Pass / Fail Criteria 12
7.6 Test Entry / Exit Criteria 12
7.7 Test Deliverables 13
7.8 Test Suspension / Resumption Criteria 13
7.9 Test Environmental / Staffing / Training Needs 13
8 STRESS TESTING 13
8.1 Test Risks / Issues 13
8.2 Items to be Tested / Not Tested 13
8.3 Test Approach(s) 13
8.4 Test Regulatory / Mandate Criteria 13
8.5 Test Pass / Fail Criteria 13
8.6 Test Entry / Exit Criteria 13
8.7 Test Deliverables 14
8.8 Test Suspension / Resumption Criteria 14
8.9 Test Environmental / Staffing / Training Needs 14
9 SYSTEM TESTING 14
9.1 Test Risks / Issues 14
9.2 Items to be Tested / Not Tested 14
9.3 Test Approach(s) 14
9.4 Test Regulatory / Mandate Criteria 14
9.5 Test Pass / Fail Criteria 14
9.6 Test Entry / Exit Criteria 14
9.7 Test Deliverables 15
9.8 Test Suspension / Resumption Criteria 15
9.9 Test Environmental / Staffing / Training Needs 15
10 UNIT TESTING 15
10.1 Test Risks / Issues 15
10.2 Items to be Tested / Not Tested 15
10.3 Test Approach(s) 15
10.4 Test Regulatory / Mandate Criteria 15
10.5 Test Pass / Fail Criteria 15
10.6 Test Entry / Exit Criteria 15
10.7 Test Deliverables 16
10.8 Test Suspension / Resumption Criteria 16
10.9 Test Environmental / Staffing / Training Needs 16
11 USER ACCEPTANCE TESTING 16
11.1 Test Risks / Issues 16
11.2 Items to be Tested / Not Tested 16
11.3 Test Approach(s) 16
11.4 Test Regulatory / Mandate Criteria 16
11.5 Test Pass / Fail Criteria 16
11.6 Test Entry / Exit Criteria 16
11.7 Test Deliverables 17
11.8 Test Suspension / Resumption Criteria 17
11.9 Test Environmental / Staffing / Training Needs 17
TEST PLAN APPROVAL 18
APPENDIX A: REFERENCES 19
APPENDIX B: KEY TERMS 20

1.INTRODUCTION
1. PURPOSE OF THE TEST PLAN DOCUMENT
	Test Plan Ensures all Functional and Design Requirements are implemented as specified in the documentation.To provide a 	procedure for various testing methos.To identify the documentation process for various testing methods.To identify the test 		methods for various testing methods.

2. COMPATIBILITY TESTING
1. TEST RISKS / ISSUES
        1. Connectivity between devices:
		For project output we use TV or monitor which need to be compatible with raspberry pi. also need to check 			compatibility between raspberry pi and our project platform.

	2.  Operating System Compatibility:
		Project should working on all operating systems as per user environment and the availability of devices with user. 			It includes operating systems like windows, linux, ubuntu. It has to perform effectively with best output.

	3. XML support:
		XML support is needed to efficiently upload the project to the server to get user required output.  

	4. Other Compatibility:
		For user guide there is a help menu which is working on browsers so it should be all browser compatible. 
		Example: Chrome, Mozilla, IE, Opera etc.

2. ITEMS TO BE TESTED / NOT TESTED
		No need to test for any android os base device. Only desktop software functionality will be tested.

3. TEST APPROACH(S)
		Without any interrupt project should run from creating new project to save project. User interface should be 		compatible or user friendly to end users. Hardware and operating system should be supported for product. 

4. TEST REGULATORY / MANDATE CRITERIA
[Describe any regulations or mandates that the system must be tested against.]
5. TEST PASS / FAIL CRITERIA
[Describe the criteria used to determine if a test item has passed or failed its test.]
6. TEST ENTRY / EXIT CRITERIA
[Describe the entry and exit criteria used to start testing and determine when to stop testing.]
7. TEST DELIVERABLES
[Describe the deliverables that will result from the testing process (documents, reports, charts, etc.).]
8. TEST SUSPENSION / RESUMPTION CRITERIA
[Describe the suspension criteria that may be used to suspend all or portions of testing. Also describe the resumption criteria that may be used to resume testing.]
9. TEST ENVIRONMENTAL / STAFFING / TRAINING NEEDS
[Describe any specific requirements needed for the testing to be performed (hardware/software, staffing, skills training, etc).)]

3. FUNCTIONAL TESTING
1. TEST RISKS / ISSUES
	There are some issues and risk in functional testing. When user creates New project with same name as existing project then 		project will not created.If project path not given proper then user has too much time to find project on computer. If end 		user gives path of system folder then their will be risk occurs. 
	When end user wants to open a existing project and end user select a project which as errors then project will not be opened.
	one you add the image/video widget you can add the resource image/video from properties window which can be first imported 		using project tools import button. if is shown in the resource table then only user can add source on image/video widget . 		Here if the image/video which is user trying to import having a different type which is not present in accepted type in 	project then image/video can not be imported. Here need to add the all extension types acceptable so user can use all types 		of images/videos.
	while widget resources are added then it is necessary to define limit on video size and image size.As per video iteration 		video will be necessary to iterate.

2. ITEMS TO BE TESTED / NOT TESTED
1. Add Screen widget:
      Description:
		After creating a new project user wants to a add screen when adding screen there is need to add screen properly with 		its height and width, if height  and width will be None then screen will not added on project.
    
       Expected Output:
		Screen should be added successfully.
       Pass Criteria:
		Screen should be added with its proper height and width.
       Fail Criteria:
		In some cases screen should not get its height and width so it not added on preview.

2. Add Image Widget:
       Description:
		adding image widget is call the function add_image_widget.at this function call one image frame should be displayed 		on screen. Selecting image widget from project tree it displays its properties on properties window.In widget resource 		window add image resource. Added resource we can see on properties window.select required image which is displayed on image 		frame.
      Expected Output:
		Image widget should be added without error and also displayed image on screen
      Pass Criteria:
		Image widget should be added.
      Fail Criteria:
		

3. Add Video Widget:
       Description:
		adding video widget is call the function add_video_widget.at this function call one video frame should be displayed 		on screen. Selecting video widget from project tree it displays its properties on properties window.In widget resource 		window add video resource. Added resource we can see on properties window.select required video which is displayed on image 		frame.
      Expected Output:
		video widget should be added without error and also displayed video on screen
      Pass Criteria:
		video widget should be added.

      Fail Criteria:
		1.when user gives iterations it not works.

4. Preview Window:
	Descrption: 
		Preview window displays the over all preview of the project which involves screen widget, Image widget and video 	widget.
	Expected Output:
		Preview window should be displayed as per design.
	Pass criteria:
	Fail Criteria:
		When preview window is maximised then screen frame is not getting resized as per window size.

5. Properties Window:
	Description:
		Properites window shows the properties of selected widget.
	Expected Output:
		It will show the properties of selected widget.
	Pass Criteria:
	Fail Criteria:
		1. When click on any textbox and press delete button then selected widget should be deleted from project tree 			window.
		2. when click  width properties text box and try to enter the specific width then another tkinter should be 			opened.
		
	       
3. TEST APPROACH(S)
		The test in this section is performed with widget type , widget resources and widgets properties. also performed 	with preview section of the project.

4. TEST REGULATORY / MANDATE CRITERIA
[Describe any regulations or mandates that the system must be tested against.]
5. TEST PASS / FAIL CRITERIA
[Describe the criteria used to determine if a test item has passed or failed its test.]

6. TEST ENTRY / EXIT CRITERIA
		Test entry criteria of the functional testing is creating new project, Opened existing project and Upload existing 	project.  after creating or uploading the project Closing project is the exit criteria of functional testing.

7. TEST DELIVERABLES
[Describe the deliverables that will result from the testing process (documents, reports, charts, etc.).]

8. TEST SUSPENSION / RESUMPTION CRITERIA
		Testing should be paused immediately if either system experiences issue while creating a new project or open a 		existing project. also testing should be paused when issue in adding images, videos and screen with its properties.

9. TEST ENVIRONMENTAL / STAFFING / TRAINING NEEDS
[Describe any specific requirements needed for the testing to be performed (hardware/software, staffing, skills training, etc).)]


4. LOAD TESTING
1. TEST RISKS / ISSUES
		Define how many screen added for one project.
		Define upper limit of image size.
		Define upper limit of video size.
		Necessary to define the video iteration limit.
		Necessary to check server memory capacity. 
		While uploading the project necessary to know internet speed and WIFI connectivity.

2. ITEMS TO BE TESTED / NOT TESTED
1. Screen Widget Limit:
		Screen widget limit should be specified to upper limit.
         Expected Output:
		Screens should be displayed on preview.
         Pass Criteria:
		Screens should be displayed on window.
         Fail Criteria:
		Many screen widget should be added. Necessary to give specific upper limit to screen widgets.
         
2. Image Size Limit:
		Image size limit should be standard as per screen size.
        Expected Output:
		Image should be displayed.
         Pass Criteria:
                 Image should displayed.
	Fail Criteria:
		width of image should not adjust

3. Video Size Limit:
		Video size limit should be specified up to 1GB.
	Expected Output:
		video should be displayed.
         Pass Criteria:
                video should displayed.
	Fail Criteria:
		width of video should not adjust

4. Video iteration limit:
		video should be continue played as per its iteration limit.
          Expected Output:
		video should be displayed.
          Pass Criteria:
                    video should displayed.
	  Fail Criteria:
		video will not iterated as per given limit

3. TEST APPROACH(S)
[Describe the overall testing approach to be used to test the project’s product. Provide an outline of any planned tests.]
4. TEST REGULATORY / MANDATE CRITERIA
[Describe any regulations or mandates that the system must be tested against.]
5. TEST PASS / FAIL CRITERIA
[Describe the criteria used to determine if a test item has passed or failed its test.]
6. TEST ENTRY / EXIT CRITERIA
[Describe the entry and exit criteria used to start testing and determine when to stop testing.]
7. TEST DELIVERABLES
[Describe the deliverables that will result from the testing process (documents, reports, charts, etc.).]
8. TEST SUSPENSION / RESUMPTION CRITERIA
[Describe the suspension criteria that may be used to suspend all or portions of testing. Also describe the resumption criteria that may be used to resume testing.]
9. TEST ENVIRONMENTAL / STAFFING / TRAINING NEEDS
[Describe any specific requirements needed for the testing to be performed (hardware/software, staffing, skills training, etc).)]


5. PERFORMANCE TESTING
1. TEST RISKS / ISSUES
[Describe the risks associated with product testing or provide a reference to a document location where it is stored. Also outline appropriate mitigation strategies and contingency plans.]
2. ITEMS TO BE TESTED / NOT TESTED
[Describe the items/features/functions to be tested that are within the scope of this test plan. Include a description of how they will be tested, when, by whom, and to what quality standards. Also include a description of those items agreed not to be tested.]
3. TEST APPROACH(S)
[Describe the overall testing approach to be used to test the project’s product. Provide an outline of any planned tests.]
4. TEST REGULATORY / MANDATE CRITERIA
[Describe any regulations or mandates that the system must be tested against.]
5. TEST PASS / FAIL CRITERIA
[Describe the criteria used to determine if a test item has passed or failed its test.]
6. TEST ENTRY / EXIT CRITERIA
[Describe the entry and exit criteria used to start testing and determine when to stop testing.]
7. TEST DELIVERABLES
[Describe the deliverables that will result from the testing process (documents, reports, charts, etc.).]
8. TEST SUSPENSION / RESUMPTION CRITERIA
[Describe the suspension criteria that may be used to suspend all or portions of testing. Also describe the resumption criteria that may be used to resume testing.]
9. TEST ENVIRONMENTAL / STAFFING / TRAINING NEEDS
[Describe any specific requirements needed for the testing to be performed (hardware/software, staffing, skills training, etc).)]


6. STRESS TESTING
1. TEST RISKS / ISSUES
		To check how many screens are added for one project.
		To check the capacity of image size.
		To verify upper limit of video size.
		To verify internet speed and WIFI connectivity for uploading the project to server.
		To verify memory limit of server for uploading the project.
2. ITEMS TO BE TESTED / NOT TESTED
[Describe the items/features/functions to be tested that are within the scope of this test plan. Include a description of how they will be tested, when, by whom, and to what quality standards. Also include a description of those items agreed not to be tested.]
3. TEST APPROACH(S)
[Describe the overall testing approach to be used to test the project’s product. Provide an outline of any planned tests.]
4. TEST REGULATORY / MANDATE CRITERIA
[Describe any regulations or mandates that the system must be tested against.]
5. TEST PASS / FAIL CRITERIA
[Describe the criteria used to determine if a test item has passed or failed its test.]
6. TEST ENTRY / EXIT CRITERIA
[Describe the entry and exit criteria used to start testing and determine when to stop testing.]
7. TEST DELIVERABLES
[Describe the deliverables that will result from the testing process (documents, reports, charts, etc.).]
8. TEST SUSPENSION / RESUMPTION CRITERIA
[Describe the suspension criteria that may be used to suspend all or portions of testing. Also describe the resumption criteria that may be used to resume testing.]
9. TEST ENVIRONMENTAL / STAFFING / TRAINING NEEDS
[Describe any specific requirements needed for the testing to be performed (hardware/software, staffing, skills training, etc).)]


7. SYSTEM TESTING
1. TEST RISKS / ISSUES
[Describe the risks associated with product testing or provide a reference to a document location where it is stored. Also outline appropriate mitigation strategies and contingency plans.]
2. ITEMS TO BE TESTED / NOT TESTED
[Describe the items/features/functions to be tested that are within the scope of this test plan. Include a description of how they will be tested, when, by whom, and to what quality standards. Also include a description of those items agreed not to be tested.]
3. TEST APPROACH(S)
[Describe the overall testing approach to be used to test the project’s product. Provide an outline of any planned tests.]
4. TEST REGULATORY / MANDATE CRITERIA
[Describe any regulations or mandates that the system must be tested against.]
5. TEST PASS / FAIL CRITERIA
[Describe the criteria used to determine if a test item has passed or failed its test.]
6. TEST ENTRY / EXIT CRITERIA
[Describe the entry and exit criteria used to start testing and determine when to stop testing.]
7. TEST DELIVERABLES
[Describe the deliverables that will result from the testing process (documents, reports, charts, etc.).]
8. TEST SUSPENSION / RESUMPTION CRITERIA
[Describe the suspension criteria that may be used to suspend all or portions of testing. Also describe the resumption criteria that may be used to resume testing.]
9. TEST ENVIRONMENTAL / STAFFING / TRAINING NEEDS
[Describe any specific requirements needed for the testing to be performed (hardware/software, staffing, skills training, etc).)]


8. UNIT TESTING
1. TEST RISKS / ISSUES
		When user adding a screen but screen not get its height and width then it will not added.
		When user added Image and video widget their frames are overwritten on each other unless you give the X and Y  			position. So user can not identify the Image and video widget.
		When we increase the preview it is necessary to increase the size of  preview section.

2. ITEMS TO BE TESTED / NOT TESTED
1.  create new project:
	Description:  
	   	1) open infoviz gui
		2) go to File -> New project
		3) give project name and project path
		4) if project name is already exists then with same name project will not be created.
		5) select finish button
	Expected Output:
	         Project will be created without any error
	Pass criteria:
		1. project will be created with new name.
	Fail Criteria:
		1. when user will give same name to project which is already existed then project will not be created. 

2. open project:
	Description:
		1. open infoviz gui
		2. go to file-> Open project
		3. select project from directory
		4. double click on project icon/name
		5. click OK button
	Expected Output:
	           Project will be opened with its properties.
	Pass Criteria:
		1.Without any innterup project will be opened successfully. 
	Fail Criteria:
		1. If project is not opened with its properties.

 3. Save project:
	Description:
		1. open infoviz gui
		2. creating new project or updating existing project to save changes in project use save option from file menu.
	Expected Output:
		project will be saved without any error.
	Pass Criteria:
		1. project is saved with its XML file.
		2. project configuration file will be generated.
	Fail Criteria:
		1. if XML files are not generated then project will not saved properly.
		2. if project configuration file is not generated then it not save properly..

4. Close Project:
	Description:
		1. Open infoviz gui
		2. if you want to close existing opened project then go to file-> close project.
	Expected Output:
		Project will be closed.
	Pass Criteria:
		1. project will be closed with its all entities.
	Fail Criteria:
		1. project will be closed with out saving changes.
		2. project close due to error occures while operating on project.

5. Add screen:
	Description:
		1. Open infoviz gui
		2. after creating project first add screen ,click on screen widget from widget toolbar
		3. add one or more screen as per requirement for each screen there is a tab with its screen name.
		4. individually user can make changes on screen
	Expected Output:
		1. screen is added with its standard height and width.
		2. screen tab is properly added on preview screen.
		3. multiple screens are added with its name on preview screen.
	Pass Criteria:
		1. XML file of screen will be generated.
	Fail Criteria:
		1. while adding screen if screen not get its height and width then screen will not be added properly.
		2. for multiple screen if multiple screen tabs are not created.

6. Add Image:
	Description:
		1. Open infoviz gui
		2. after selecting on screen user can add image widget on that screen, click on image widget from widget toolbar
		3. add one or more image widget as per requirement.
		4. individually user can make changes on image widget.
	Expected Output:
		1. image widget should be added with is standard height and width.
		2. Image frame will be added on screen.
		3. In case of multiple screen image widget frame is added on selected screen. 
	Pass Criteria:
		1. XML file of image will be generated with its properties.
	Fail Criteria:
		1. In case of multiple screens image widget frame is not added on selected screen
		2. Frame size should not be greater than screen size.

7. Add Video:
	Description:
		1. Open infoviz gui
		2. after selecting on screen user can add video widget on that screen, click on video widget from widget toolbar
		3. add one or more video widget as per requirement.
		4. individually user can make changes on video widget.
	Expected Output:
		1. video widget should be added with is standard height and width.
		2. video frame will be added on screen.
		3. In case of multiple screen video widget frame is added on selected screen.
	Pass Criteria:
		1. XML file of video will be generated with its properties.
	Fail Criteria:
		1. In case of multiple screens video widget frame is not added on selected screen
		2. Frame size should not be greater than screen size.

8. Add Resources:
	Description:
		1. Open infoviz gui
		2. to import images or video click on import button from widget resources.
		3. select images and video from directory.
		4. image size should be standard screen size.
		5. video size should be upto 1 GB.
	Expected Output:
		Added resources that is image and video should be display on widget resources window with its type, name ,extension, 			width, height, duration and size.
	Pass Criteria:
		1. Resource will be added with its all properties.
	Fail Criteria:
		1. if resource size is greater than expected size then resources are not added on resource list. 

9. Delete Resources:
	Description:
		1. open infoviz gui
		2. to delete imported resources ,first select resources from list 
		3. click on delete button from widget resources.
	Expected Output:
		Resources will be deleted
	Pass Criteria:
		Resources should be deleted from its path and from resource list of widget resources.
	Fail Criteria:
		While deleteting resources if resouces not get its path then it should not be deleted from resource list.

10. properties window:
	Description:
		1. open infoviz gui
		2. when user click on widget its property should be display on properties window.
		3. user can changes widget properties as per requirements.
	Expected Output:
		updated properties of widget should be impact on preview screen.

11. Project Tree:
	Description:
		1. open infoviz gui
		2. selected widget are added into project tree.
		3. when user want to delete added widget then first select widget from project tree.
		4. then click on delete icon which are shown in the window of  project tree.
	Expected Output:
		Project tress is update with added widget.

12. Upload Project:
	Description:
		1. open infoviz gui
		2. go to tools -> upload project.
		3. fill the all required details and upload the project.
Not Tested:
	No need to test for any android os base device. Only desktop software functionality will be tested.

3. TEST APPROACH(S)
		Tests will be conducted as per the documented test cases stored in TestLodge.The tester will execute the tests in 	TestLodge and mark each case as Pass / Fail / Skip. The tester should leave notes on actual results and any other relevant 	details when possible. 
		When tests are marked as Fail, bug reports will automatically be created in the issue tracker integrated with 		TestLodge.Once complete, the manager should review the test run reports in TestLodge and report back to the team accordingly.

4. TEST REGULATORY / MANDATE CRITERIA
[Describe any regulations or mandates that the system must be tested against.]
5. TEST PASS / FAIL CRITERIA
		All core functionality of the unit should function as expected and outlined in the individual test cases. There must 	be no critical defects found and an end user must be able to complete a project cycle successfully from New Project to Save 		Project. 95% of all test cases should pass and no failed cases should be crucial to the end-user’s ability to use the application.
6. TEST ENTRY / EXIT CRITERIA
[Describe the entry and exit criteria used to start testing and determine when to stop testing.]
7. TEST DELIVERABLES
		Upon completion, the test run results will be saved in TestLodge and the test manager should then run a report for all completed tests.
8. TEST SUSPENSION / RESUMPTION CRITERIA
		Testing should be paused immediately if either system experiences issue while creating a new project or open a existing project.
9. TEST ENVIRONMENTAL / STAFFING / TRAINING NEEDS
		The project must be populated with  a widgets with its properties. Test mode should be enabled for the developers.


TEST PLAN APPROVAL
The undersigned acknowledge they have reviewed the <Project Name> Test Plan document and agree with the approach it presents. Any changes to this Requirements Definition will be coordinated with and approved by the undersigned or their designated representatives.
[List the individuals whose signatures are required.  Examples of such individuals are Business Steward, Technical Steward, and Project Manager. Add additional signature lines as necessary.]

Signature:
Date:
Print Name:
Title:
Role:
Signature:
Date:
Print Name:
Title:
Role:
Signature:
Date:
Print Name:
Title:
Role:
Appendix A: References
[Insert the name, version number, description, and physical location of any documents referenced in this document.  Add rows to the table as necessary.] 
The following table summarizes the documents referenced in this document.
Document Name and Version
Description
Location
<Document Name and Version Number>
[Provide description of the document]
<URL or Network path where document is located>
Appendix B: Key Terms
[Insert terms and definitions used in this document.  Add rows to the table as necessary. Follow the link below to for definitions of project management terms and acronyms used in this and other documents.
http://www2.cdc.gov/cdcup/library/other/help.htm
The following table provides definitions for terms relevant to this document.
Term
Definition
[Insert Term]
[Provide definition of the term used in this document.]
[Insert Term]
[Provide definition of the term used in this document.]
[Insert Term]
[Provide definition of the term used in this document.]


